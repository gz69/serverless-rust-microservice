// Import necessary libraries and modules
use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use std::collections::HashMap;
use rand::prelude::*;

// Define a struct to represent the request payload received by the Lambda function
#[derive(Deserialize)]
struct Request {
    company: Option<String>,
    employee_role: Option<String>,
}

// Define a struct to represent the response payload to be returned by the Lambda function
#[derive(Serialize)]
struct Response {
    name: String,
    employee_id: Option<u64>,
}

// The main entry point for the Lambda function, utilizing the `tokio` runtime
#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    // Create a service function using the handler function and run the Lambda function
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

// The handler function responsible for processing Lambda events
async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    // Deserialize the incoming JSON payload into the Request struct
    let request: Request = serde_json::from_value(event.payload)?;

    // Load AWS configuration from environment variables
    let config = load_from_env().await;
    // Create a DynamoDB client using the loaded configuration
    let client = Client::new(&config);

    // Invoke the search_employee function to retrieve employee information based on the request
    let (employee_name, employee_id) = search_employee(&client, request.company, request.employee_role).await?;

    // Serialize the response into JSON format and return it
    Ok(json!({
        "employee_id": employee_id,
        "name": employee_name,
    }))
}

// Function to search for employee information in DynamoDB based on provided parameters
async fn search_employee(client: &Client, company: Option<String>, employee_role: Option<String>) -> Result<(String, Option<u64>), LambdaError> {
    // Define the DynamoDB table name
    let table_name = "EmployeeInfo";
    // Initialize HashMap for expression attribute values and a String for filter expression
    let mut expr_attr_values = HashMap::new();
    let mut filter_expression = String::new();

    // Check if company parameter is provided in the request
    if let Some(company_val) = company {
        // Insert company parameter into expression attribute values
        expr_attr_values.insert(":company_val".to_string(), AttributeValue::S(company_val));
        // Append filter condition for the company to the filter expression
        if !filter_expression.is_empty() {
            filter_expression.push_str(" and ");
        }
        filter_expression.push_str("company = :company_val");
    }

    // Check if employee_role parameter is provided in the request
    if let Some(employee_role_val) = employee_role {
        // Append filter condition for the employee_role to the filter expression
        if !filter_expression.is_empty() {
            filter_expression.push_str(" and ");
        }
        expr_attr_values.insert(":employee_role_val".to_string(), AttributeValue::S(employee_role_val));
        filter_expression.push_str("employee_role = :employee_role_val");
    }

    // Perform a DynamoDB scan operation with the constructed parameters
    let result = client.scan()
        .table_name(table_name)
        .set_expression_attribute_values(Some(expr_attr_values))
        .set_filter_expression(Some(filter_expression))
        .send()
        .await?;

    // Extract the items from the scan result
    let items = result.items.unwrap_or_default();

    // Randomly choose an item from the scan result
    let selected_item = items.iter().choose(&mut thread_rng());

    // Match and extract relevant attributes from the selected item
    match selected_item {
        Some(item) => {
            let name_attr = item.get("name").and_then(|val| val.as_s().ok());
            let id_attr = item.get("employee_id").and_then(|val| val.as_n().ok().map(|id| id.parse().ok()).flatten());

            // Match the extracted attributes and return the result
            match (name_attr, id_attr) {
                (Some(name), Some(id)) => Ok((name.to_string(), Some(id))),
                _ => Err(LambdaError::from("No name or employee_id found in the selected item")),
            }
        },
        None => Err(LambdaError::from("No matching employee information found")),
    }
}

