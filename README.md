# Serverless Rust Microservice for Employee Data Retrieval

## Project Overview

This project is focused on building a serverless Rust microservice using AWS Lambda. The microservice is designed to retrieve employee information based on company and role parameters, demonstrating the seamless integration with a DynamoDB database. This document provides detailed instructions on project setup, deployment, and demonstration.

## Getting Started

### Prerequisites

- Rust and Cargo installed on your system.
- AWS CLI configured with Administrator access.
- cargo-lambda installed for Lambda function management.

### Installation

1. **Install cargo-lambda (if not already installed):**

    ```bash
    brew tap cargo-lambda/cargo-lambda
    brew install cargo-lambda
    ```

2. **Create a new Cargo Lambda project:**

    ```bash
    cargo lambda new yc557-week5-mini-project && cd yc557-week5-mini-project
    ```

### Configuration

Edit `main.rs` and `Cargo.toml` as needed to implement the functionality for fetching employee names from a DynamoDB database based on company and role parameters.

### Deployment

Deploy your project to AWS Lambda using the following commands:

```bash
cargo lambda build --release
cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::471112660632:role/ids721

**AWS Setup**
DynamoDB Table Creation
Navigate to the DynamoDB section in the AWS Console and create a new table named `EmployeeInfo`.

Populate the table with data entries. Example:

```json
{
  "name": { "S": "Sundar Pichai" },
  "company": { "S": "Google" },
  "employee_role": { "S": "CEO" },
  "employee_id": { "N": "11" }
}
```

**API Gateway Configuration**
Create an API Gateway trigger for the Lambda function.
Set up a new REST API and create a resource named `week5-resource`.
Deploy the API and create a stage named `test`.

**Demonstration**
Role Permissions
Ensure the IAM role associated with the AWS Lambda function has `AmazonDynamoDBFullAccess`, `AWSLambda_FullAccess`, and `AWSLambdaBasicExecutionRole` policies attached.

**Testing the Service**
Use Postman or a similar tool to send POST requests to:

```bash
https://64utey4gk4.execute-api.us-east-1.amazonaws.com/test/yc557-week5-mini-project/week5-resource
```
Include parameters for `company` and `employee_role` to fetch the relevant employee's ID and name.

**Conclusion**

This project showcases the use of Rust in serverless computing, emphasizing Rust Lambda functionality, efficient database integration, and the practical implementation of a microservice. Detailed documentation and visual evidence highlight the project's architecture and functionality, ensuring clarity and understanding of its impact.
